const express = require('express');
const path = require('path');
const fs = require('fs');
const app = express();

app.use(express.static('/build'));

app.get('/*', function (req, res) {
  res.sendFile(path.join('/build/index.html'));
});

try {
  const serverEnv = process.env.REACT_PUBLIC_SERVER_ENV;

  if (serverEnv) {
    const indexHtml = fs
      .readFileSync('/build/index.html')
      .toString('utf8');

    const indexHtmlReplacedServerEnv = indexHtml
      .replace('window.REACT_PUBLIC_SERVER_ENV={}', `window.REACT_PUBLIC_SERVER_ENV = ${serverEnv};`);

    fs.writeFileSync('/build/index.html', indexHtmlReplacedServerEnv);
  }
} catch (error) {
  console.log('REACT_PUBLIC_SERVER_ENV replacement failed:', error);
}

app.listen(8080);

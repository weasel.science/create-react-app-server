FROM node:16
RUN mkdir /server
WORKDIR /server
ADD index.js index.js
RUN npm install express
CMD node index.js
